Client:	Client.c
	gcc	Client.c -Werror -Wall -pedantic -o Client

Server: Server.c        
	gcc	Server.c -Werror -Wall -pedantic -o Server

clean:
	rm	-f	*.o	Client
	rm	-f	*.o	Server
