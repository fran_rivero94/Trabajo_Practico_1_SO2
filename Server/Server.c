#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <ctype.h>
#include <arpa/inet.h>
#include "command.h"
#include "parser.h"

#define TAM 1024
/*Definicion de funciones a utilizar*/
	void login();
	void start();
	void disconnect();
	void mainloop();
	void set_pipes(char*);

/*Definicion de variables para las conexiones tanto UDP, como TCP*/
	struct sockaddr_in cli_addr;
/*Utilizo estructura data para almacenar los datos relevantes del programa*/
    struct data {
    int flag;
    int p;
    char port[TAM];
	char username[TAM];
	char update_date[TAM];
	char message[TAM];
	char buffer[TAM];
	} data;

int main() 
{
	start();
	login();

	return EXIT_SUCCESS; 
} 
void login()
{
	/*Definicion de variables para el servidor TCP*/
	int sockfd, newsockfd,  puerto, pid, n,i;
	char buffer[TAM],buf[TAM];
	socklen_t clilen;
	char* pass = NULL;
	struct sockaddr_in serv_addr;
	/*Se inicicializa el servidor TCP*/
	sockfd = socket( AF_INET, SOCK_STREAM, 0);

	if ( sockfd < 0 ) { 
		perror( " apertura de socket ");
		exit( 1 );
	}

	memset( (char *) &serv_addr, 0, sizeof(serv_addr) );
	puerto = atoi( data.port);
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons( puerto );

	/*Se conecta el servidor a la direccion dada*/

	if ( bind(sockfd, ( struct sockaddr *) &serv_addr, sizeof( serv_addr ) ) < 0 ) {
		perror( "ligadura" );
		exit( 1 );
	}
	listen( sockfd, 5 );
	clilen = sizeof( cli_addr );

 	/*Se acepta un nuevo cliente en el servidor*/

	while(1)
	{
		newsockfd = accept( sockfd, (struct sockaddr *) &cli_addr, &clilen );
		if ( newsockfd < 0 ) {
			perror( "accept" );
			exit( 1 );
		}

		pid = fork(); 
		if ( pid < 0 ) {
			perror( "fork" );
			exit( 1 );
		}

		if ( pid == 0 ) 
		{  
			close( sockfd );
			
			/*Se pregunta al cliente y se envia si es correcto o no el nombre de usuario*/

			printf("Expecting username\n");
			memset( buffer, 0, TAM );

			n = read( newsockfd, buffer, TAM-1 );
			printf("%s\n",buffer );
			if ( n < 0 ) {
				perror( "lectura de socket" );
				exit(1);
			}
			else{
				buffer[strlen(buffer) - 1] = '\0';
				printf("%s\n",buffer );
				i = strlen(buffer);
		        printf("User received %s of length %d\n", buffer, i);
			}
			printf("Received\n");
			if (!strcmp("franco", buffer) ) {
		        strcpy(data.username,buffer);
		        printf("Username entered is correct\n");
		        snprintf(buffer, sizeof(buffer), "CORRECT");         		
		    }
		    else {
		        printf("Incorrect Username %s\n", buffer);
		        snprintf(buffer, sizeof(buffer), "I");
		        exit(1);
		    }
		    printf("SENDING : %s\n",buffer);
			n = write( newsockfd, buffer, TAM-1 );
			if ( n < 0 ) {
				perror( "ERROR: sending messge" );
				exit( 1 );
			}
			/*Se pregunta al cliente y se envia si es correcto o no, la contraseña*/

			printf("Expecting the password\n");
			memset( buffer, 0, TAM );
			n = read( newsockfd, buffer, TAM-1 );
			printf("%s\n",buffer );
			if ( n < 0 ) {
				perror( "lectura de socket" );
				exit(1);
			}
			else{
				buffer[strlen(buffer) - 1] = '\0';
				printf("%s\n",buffer );
				i = strlen(buffer);
		        printf("Password received %s of length %d\n", buffer, i);
			}
			printf("Received\n");
			if (!strcmp("rivero", buffer) ) {
		        printf("Password entered is correct\n");
		        snprintf(buffer, sizeof(buffer), "CORRECT");
		    }
		    else {
		        printf("Incorrect Password %s\n", pass);
		        snprintf(buffer, sizeof(buffer), "I");
		        exit(1);
		    }
		    printf("SENDING : %s\n",buffer);
			n = write( newsockfd, buffer, TAM-1 );
			if ( n < 0 ) {
				perror( "ERROR: sending messge" );
				exit( 1 );
			}
			printf("connection accepted %s\n", data.port);
			
			/*Se inicia la conexion TCP*/

			while(1)
			{
				memset( buf, 0, TAM );

				/*Se recibe el comando desde el cliente y se decide que hacer con ese comando*/

				n = read( newsockfd, buf, TAM-1 );
				if ( n < 0 ) 
				{
					perror( "reading to socket" );
					exit(1);
				}
				buf[strlen(buf)] = '\0';
				//printf("%s\n",buf );
				//mainloop();
				//char input[256];
				
				/*
				* Muestra el hostname y el directorio actual
				* Queda espectante al input como el baash
				* Y ejeucta funciones para primero interpretar el comando y despues ejecutarlo
				*/

				char currentdir[1024];
				if (getcwd(currentdir, sizeof(currentdir)) == NULL)
				{//El directorio de trabajo
				       printf("getcwd() error");
				}

				printf("%s@%s:~%s$ ", data.username, getenv("HOSTNAME"), currentdir);//Muestra nombre y posicion actual
				set_pipes(buf);

				n = write( newsockfd, "Obtuve su mensaje", 18 );
				if ( n < 0 ) 
				{
					perror( "writing to socket" );
					exit( 1 );
				}
				/*n = write( newsockfd, data.message, TAM-1 );
				if ( n < 0 ) 
				{
					perror( "writing to socket" );
					exit( 1 );
				}*/
					
			}
		}
		else 
		{
			printf( "SERVIDOR: Nuevo cliente, que atiende el proceso hijo: %d\n", pid );
			close( newsockfd );
		}
	}
}

/*funciona como inicio de la conexion tcp, consiguiendo los parametros necesarios para que se realice la conexion*/
void start()
{

	/*se declaran las variables a utilizar para la conexion al servidor*/

	char comando_inicial[TAM];    
    /*se da un mensaje de bienvenida al servidor*/

    printf("welcome to server\n");
    printf("insert connect to start\n");
    printf(">");
	memset( comando_inicial, '\0', TAM );
	fgets (comando_inicial, TAM-1, stdin);
	comando_inicial[strlen(comando_inicial)-1] = '\0';

	/*Si se recibe la cadena connect, y se toma un puerto por defecto el puerto 6020 */

	if(!strcmp( "connect", comando_inicial ))
	{
		strcpy(data.port,"6020");
		fprintf( stderr, "Uso: %s <port>", data.port );
		printf("\n\n");
	}

	else{
		perror( "INVALID FORMAT ERROR: expect connect " );
		exit( 1 );    
	}
}

/*Desconexion del servidor*/

void disconnect()
{
	printf("client %s disconnected \n",data.username);
	exit(0);
}

/*
 * Separa los comandos segun pipes, crea los pipes y ejecuta los comandos
 */
void set_pipes(char *input_string)
{
	struct parser_element commands[10];// Obtiene la secuencia de comandos en pipes
	int num_commands=0;

	parse_pipes(input_string, &num_commands, &commands[0]);

	int pipes[num_commands][2];
	//creo todos los pipes
	int i;
	for(i=0; i < num_commands-1; i++){
		if(pipe(pipes[i])<0) {
			perror("Error creating pipe!");
			exit(1);
		}
	}

	int counter=0;
	int pid;
	while(counter < num_commands){
		//Checkeo que no sea builtin
		if(!built_in_command(&commands[counter])){
			pid = fork();
			if (pid==0){
				if(!counter==0){//Si no es el primer comando
					if(dup2(pipes[counter-1][0],0)< 0){
						perror("Error with dup!");
					}
				}
				if(!(counter==(num_commands-1))){ //Si no es el ultimo comando
					if(dup2(pipes[counter][1],1) < 0){
							perror("Error with dup!");
					}
				}
				command_search_logic(&commands[counter]);
				exit(0);
			}
			if(!commands[counter].background_process)
				wait(0);
			if(counter >0)
				close(pipes[counter-1][0]);
			if(counter<(num_commands-1))
				close(pipes[counter][1]);
		}
		counter++;
	}
}